package com.buzzhiraassessment;

import android.animation.LayoutTransition;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.buzzhiraassessment.CalendarDecorators.EventDecorator;
import com.buzzhiraassessment.CalendarDecorators.TodayCalendarDecorator;
import com.buzzhiraassessment.Exercise1.MathUtils;
import com.buzzhiraassessment.SecundaryStuff.CreateEventDialog;
import com.buzzhiraassessment.SecundaryStuff.EventsDayDialog;
import com.buzzhiraassessment.SecundaryStuff.MonthEvent;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 *
 *                      ***    DISCLOSURE   ***
 *
 *
 * Note that I didn't dedicate the time to make the code more readable and organized
 * as I would have in a real project.
 *
 *
 *                       ***   PROCEED :)   ***
 *
 *
 */

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private WeekView calendar;
    private MaterialCalendarView calendarMonthly;
    private ArrayList<WeekViewEvent> eventsArray;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FloatingActionButton fab;
    private ArrayList<MonthEvent> fullEventsList;
    private ArrayList<CalendarDay> daysList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         * Test Exercise 1 solution
         */
        Toast.makeText(this, "" + MathUtils.solution(125), Toast.LENGTH_LONG).show();


        initializations();
        new loadEvents().execute();
        setupDrawer();
        setupCalendar();



    }

    void initializations() {
        eventsArray = new ArrayList<>();
        fullEventsList = new ArrayList<>();
        daysList = new ArrayList<>();
        toolbar = (Toolbar)findViewById(R.id.mToolbar);
        RelativeLayout mainContainer = (RelativeLayout) findViewById(R.id.activity_main);
        calendar = (WeekView)findViewById(R.id.weekView);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        calendarMonthly = (MaterialCalendarView) findViewById(R.id.calendarMonthView);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        setSupportActionBar(toolbar);

        LayoutTransition layoutTransition = new LayoutTransition();
        layoutTransition.setDuration(100);
        mainContainer.setLayoutTransition(layoutTransition);
    }

    /**
     * Calendar
     */

    void setupCalendar() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CreateEventDialog().show(getFragmentManager(), "");

            }
        });


        /**
         * weekly calendar
         */

        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        calendar.goToHour(currentHour > 3 ? currentHour - 3 : 0);

        calendar.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {

                List<WeekViewEvent> matchedEvents = new ArrayList<>();
                for (WeekViewEvent event : eventsArray) {
                    if (eventMatches(event, newYear, newMonth)) {
                        matchedEvents.add(event);
                    }
                }
                return matchedEvents;
            }
        });

        calendar.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {

                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE d/M", Locale.getDefault());

                return weekdayNameFormat.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour, int minutes) {
                return (hour < 10 ? "0" : "") +  hour + ":" + minutes + "0";
            }
        });


        /**
         * monthly calendar
         */

        calendarMonthly.setTileHeightDp(60);
        calendarMonthly.setSelectionColor(ContextCompat.getColor(this, R.color.colorPrimary));
        TodayCalendarDecorator todayDecorator = new TodayCalendarDecorator();
        todayDecorator.setDate(Calendar.getInstance().getTime());
        calendarMonthly.addDecorator(todayDecorator);

    }

    public void addNewEvent(Calendar time, String name) {

        Calendar endTime = (Calendar)time.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 1);
        WeekViewEvent event = new WeekViewEvent(eventsArray.size()+10, name, time, endTime);
        event.setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        eventsArray.add(event);
        calendar.notifyDatasetChanged();



        ArrayList<CalendarDay> dayList = new ArrayList<>();
        CalendarDay dayy = CalendarDay.from(time);
        dayList.add(dayy);

        final EventDecorator eventDecorator = new EventDecorator(dayList, MainActivity.this);
        calendarMonthly.addDecorator(eventDecorator);

        String startingString = ((time.get(Calendar.HOUR_OF_DAY) < 10) ? "0" + time.get(Calendar.HOUR_OF_DAY) : time.get(Calendar.HOUR_OF_DAY))
                + ":" + ((time.get(Calendar.MINUTE) < 10) ? "0" + time.get(Calendar.MINUTE) : time.get(Calendar.MINUTE));

        MonthEvent monthEvent = new MonthEvent(CalendarDay.from(time), startingString, ContextCompat.getColor(this, R.color.colorPrimaryDark), name);
        fullEventsList.add(monthEvent);
        daysList.add(dayy);

        setMonthlyCalendarListener();



    }

    public class loadEvents extends AsyncTask<String, String, String> {

        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL("https://assessments.bzzhr.net/calendar/?format=json");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }


            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            JSONArray jsonArray;                                //2016-11-21T17:00:00Z
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());

            try {
                jsonArray = new JSONArray(result);

                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonObj = jsonArray.getJSONObject(i);

                    Calendar startTime = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault()) ;
                    Calendar endTime = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault()) ;

                    try {

                        Date startDate = dateFormat.parse(jsonObj.getString("start"));
                        startDate.setTime(startDate.getTime()-1);
                        startTime.setTime(startDate);

                        Date endDate = dateFormat.parse(jsonObj.getString("end"));
                        endDate.setTime(endDate.getTime()-1);
                        endTime.setTime(endDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    WeekViewEvent newEvent = new WeekViewEvent(i, jsonObj.getString("label"), startTime, endTime);
                    newEvent.setColor(decodeColor(jsonObj.getString("category")));

                    eventsArray.add(newEvent);


                    CalendarDay day = CalendarDay.from(startTime);
                    boolean isRepeated = false;
                    for (CalendarDay calendarDay : daysList) {
                        if (calendarDay.getDay() == day.getDay() &&
                                calendarDay.getMonth() == day.getMonth() &&
                                calendarDay.getYear() == day.getYear()) {
                            isRepeated = true;
                        }
                    }

                    if (!isRepeated)
                        daysList.add(day);

                    String startingString = ((startTime.get(Calendar.HOUR_OF_DAY) < 10) ? "0" + startTime.get(Calendar.HOUR_OF_DAY) : startTime.get(Calendar.HOUR_OF_DAY))
                            + ":" + ((startTime.get(Calendar.MINUTE) < 10) ? "0" + startTime.get(Calendar.MINUTE) : startTime.get(Calendar.MINUTE));

                    MonthEvent monthEvent = new MonthEvent(day, startingString, decodeColor(jsonObj.getString("category")), jsonObj.getString("label"));
                    fullEventsList.add(monthEvent);

                }

                final EventDecorator eventDecorator = new EventDecorator(daysList, MainActivity.this);
                calendarMonthly.addDecorator(eventDecorator);
                setMonthlyCalendarListener();

                calendar.notifyDatasetChanged();

            } catch (JSONException e) {
                Toast.makeText(MainActivity.this, "Couldn't load events. Check your internet connection.", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }


        }

    }

    void setMonthlyCalendarListener() {
        calendarMonthly.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                if (daysList.contains(date)) {
                    EventsDayDialog eventsDayDialog = new EventsDayDialog();
                    Bundle bundle = new Bundle();
                    String dateString = date.getDay() + "/" + date.getMonth();
                    int eventsAmount = 0;
                    bundle.putString("date", dateString);

                    for (MonthEvent monthEvent : fullEventsList) {
                        if (monthEvent.date.getDay() == date.getDay() &&
                                monthEvent.date.getMonth() == date.getMonth() &&
                                monthEvent.date.getYear() == date.getYear()) {
                            eventsAmount = eventsAmount + 1;

                            bundle.putString("name" + eventsAmount,  monthEvent.name);
                            bundle.putInt("color" + eventsAmount, monthEvent.color);
                            bundle.putString("time" + eventsAmount, monthEvent.time);
                        }
                    }

                    bundle.putInt("eventsAmount", eventsAmount);
                    eventsDayDialog.setArguments(bundle);
                    eventsDayDialog.show(getSupportFragmentManager(), "");
                }
            }
        });
    }

    void setupDrawer() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(mDrawerToggle);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull final MenuItem item) {

                drawer.closeDrawers();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        switch (item.getItemId()) {

                            case R.id.day1:
                                showMonthlyCalendar(false);
                                shrinkCalendarHeaderText(false);
                                calendar.setNumberOfVisibleDays(1);
                                break;

                            case R.id.day3:
                                showMonthlyCalendar(false);
                                shrinkCalendarHeaderText(false);
                                calendar.setNumberOfVisibleDays(3);
                                break;

                            case R.id.weekly:
                                showMonthlyCalendar(false);
                                shrinkCalendarHeaderText(true);
                                calendar.setNumberOfVisibleDays(7);
                                break;

                            case R.id.monthly:
                                showMonthlyCalendar(true);
                                break;
                        }

                    }
                }, 250);

                return true;
            }
        });

        mDrawerToggle.syncState();
        navigationView.getMenu().getItem(1).setChecked(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    void showMonthlyCalendar(boolean yesPlease) {

        if (yesPlease) {
            calendar.setVisibility(View.GONE);
            calendarMonthly.setVisibility(View.VISIBLE);
        }
        else {
            calendarMonthly.setVisibility(View.GONE);
            calendar.setVisibility(View.VISIBLE);
        }

    }

    void shrinkCalendarHeaderText(final boolean yesPlease) {
        calendar.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                if (yesPlease) {
                    SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("d/M", Locale.getDefault());

                    return weekdayNameFormat.format(date.getTime());
                }
                else {
                    SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE d/M", Locale.getDefault());

                    return weekdayNameFormat.format(date.getTime());
                }
            }

            @Override
            public String interpretTime(int hour, int minutes) {
                return (hour < 10 ? "0" : "") +  hour + ":" + minutes + "0";
            }
        });

        if (yesPlease) {
            calendar.setColumnGap(getDP(2));
            calendar.setTextSize(getDP(10));
            calendar.setEventTextSize(getDP(10));
        }
        else {
            calendar.setColumnGap(getDP(8));
            calendar.setTextSize(getDP(12));
            calendar.setEventTextSize(getDP(12));
        }
    }

    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month-1) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }

    private int decodeColor(String color) {

        int decodedColor = ContextCompat.getColor(this, R.color.colorPrimary);

        switch (color) {

            case "black":
                decodedColor = Color.BLACK;
                break;

            case "blue":
                decodedColor = ContextCompat.getColor(this, R.color.colorPrimary);
                break;

            case "cyan":
                decodedColor = Color.parseColor("#0097A7");
                break;

            case "grey":
                decodedColor = Color.GRAY;
                break;

            case "green":
                decodedColor = Color.parseColor("#43A047");
                break;

            case "red":
                decodedColor = Color.parseColor("#E53935");
                break;

            case "white":
                decodedColor = ContextCompat.getColor(this, R.color.colorAccent);
                break;

            case "yellow":
                decodedColor = Color.parseColor("#FDD835");
                break;

            case "pink":
                decodedColor = Color.parseColor("#F06292");
                break;

        }

        return decodedColor;
    }

    private int getDP(int i) {
        return (int)  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, getResources().getDisplayMetrics());
    }


}
