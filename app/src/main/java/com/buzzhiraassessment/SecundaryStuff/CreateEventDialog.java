package com.buzzhiraassessment.SecundaryStuff;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.buzzhiraassessment.MainActivity;
import com.buzzhiraassessment.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/*
 * Created by João on 31/10/2016.
 */

public class CreateEventDialog extends DialogFragment {

    Button create;
    Button cancel;
    EditText newEventText;
    Calendar eventDate;
    TextView dateText, timeText;
    LinearLayout root;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_new_calendar_event, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        create = (Button)view.findViewById(R.id.create_event);
        cancel = (Button)view.findViewById(R.id.cancel_new_event);
        newEventText = (EditText)view.findViewById(R.id.newEventText);
        dateText = (TextView)view.findViewById(R.id.dateText);
        timeText = (TextView)view.findViewById(R.id.timeText);
        root = (LinearLayout)view.findViewById(R.id.new_event_root);
        eventDate = Calendar.getInstance();

        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        eventDate.set(Calendar.YEAR, i);
                        eventDate.set(Calendar.MONTH, i1);
                        eventDate.set(Calendar.DAY_OF_MONTH, i2);
                        setTexts(eventDate);
                    }
                }, eventDate.get(Calendar.YEAR), eventDate.get(Calendar.MONTH), eventDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        timeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        eventDate.set(Calendar.HOUR_OF_DAY, i);
                        eventDate.set(Calendar.MINUTE, i1);
                        setTexts(eventDate);
                    }
                }, eventDate.get(Calendar.HOUR_OF_DAY), eventDate.get(Calendar.MINUTE), true).show();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newEventText.getText().toString().length() > 0) {

                    ((MainActivity)getActivity()).addNewEvent(eventDate, newEventText.getText().toString());
                    dismiss();
                }
                else {
                    Toast.makeText(getActivity(), "Event must have a name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        root.setMinimumWidth((int) (getScreenWidth() * .9f));
        setTexts(eventDate);
        newEventText.setLines(1);
        newEventText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        newEventText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        return view;
    }

    public int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    void setTexts(Calendar cal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        dateText.setText(dateFormat.format(cal.getTime()));
        SimpleDateFormat hourFormat = new SimpleDateFormat("kk:mm");
        timeText.setText(hourFormat.format(cal.getTime()));
    }

}

