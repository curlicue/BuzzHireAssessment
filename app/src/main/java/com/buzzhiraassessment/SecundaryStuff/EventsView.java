package com.buzzhiraassessment.SecundaryStuff;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.buzzhiraassessment.R;

/**
 * João was here on 22/12/2016.
 */

public class EventsView extends RelativeLayout {

    TextView nameText;
    TextView timeText;
    ImageView colorImg;

    public EventsView(Context context, int color, String name, String time) {
        super(context);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.view_events, this, true);

        nameText = (TextView)findViewById(R.id.eventViewName);
        timeText = (TextView)findViewById(R.id.eventViewHour);
        colorImg = (ImageView)findViewById(R.id.eventsColorView);

        nameText.setText(name);
        timeText.setText(time);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            colorImg.setBackground(makeBackgroundCircle(color));
        else
            colorImg.setBackgroundDrawable(makeBackgroundCircle(color));

    }

    Drawable makeBackgroundCircle(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setColor(color);
        return shape;
    }



}
