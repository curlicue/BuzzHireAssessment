package com.buzzhiraassessment.CalendarDecorators;

/**
 * João was here on 22/12/2016.
 */


import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.buzzhiraassessment.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;


public class EventDecorator implements DayViewDecorator {

    private HashSet<CalendarDay> dates;
    Context mContext;

    public EventDecorator(Collection<CalendarDay> dates , Context context) {
        this.mContext = context;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new CustomDotSpan(ContextCompat.getColor(mContext, R.color.colorPrimaryDark), 0, mContext));

    }
}